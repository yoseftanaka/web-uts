<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class UserActionController extends Controller
{
    public function index(){
        $users = User::paginate(5);
        // $users = DB::table('users')->get();
        return view('view_all_users',compact('users'));
    }

    public function show($id){
        $user = User::find($id);
        return view('view_a_user',compact('user'));
    }
}
